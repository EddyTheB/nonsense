from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
  url(r'^admin/', admin.site.urls),
  url(r'^data/', include('data_feed.urls')),
  url(r'^meteorology/', include('meteorology.urls')),
  #url(r'^wedding/', include('wedding.urls'))
  #url(r'^wedding/', include('wedding.urls')),
  url(r'^', include('pages.urls')),
]

from __future__ import print_function

import os

import argparse
from datetime import datetime, timedelta
import pytz
import urllib #.request
import json
import pandas as pd
import numpy as np
from fuzzywuzzy import process

import django
os.environ["DJANGO_SETTINGS_MODULE"] = 'nonsense.settings'
django.setup()
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from meteorology.models import met_layer, met_obs, met_locations

import tools

Params = dict()
Params['G'] = {u'units': u'mph',     u'name': u'G',  u'$': u'Wind Gust'}
Params['T'] = {u'units': u'C',       u'name': u'T',  u'$': u'Temperature'}
Params['V'] = {u'units': u'm',       u'name': u'V',  u'$': u'Visibility'}
Params['D'] = {u'units': u'compass', u'name': u'D',  u'$': u'Wind Direction'}
Params['S'] = {u'units': u'mph',     u'name': u'S',  u'$': u'Wind Speed'}
Params['W'] = {u'units': u"",        u'name': u'W',  u'$': u'Weather Type'}
Params['P'] = {u'units': u'hpa',     u'name': u'P',  u'$': u'Pressure'}
Params['Pt'] = {u'units': u'Pa/s',   u'name': u'Pt', u'$': u'Pressure Tendency'}
Params['Dp'] = {u'units': u'C',      u'name': u'Dp', u'$': u'Dew Point'}
Params['H'] = {u'units': u'%',       u'name': u'H',  u'$': u'Screen Relative Humidity'}
Params['$'] = {u'units': u'minutes', u'name': u'$',  u'$': u'Minute Of Day'}
Params['F'] = {u'units': u'C',       u'name': u'F',  u'$': u'Feels Like Temperature'}
Params['U'] = {u'units': u'',        u'name': u'U',  u'$': u'Max UV Index'}
Params['Pp'] = {u'units': u'%',      u'name': u'Pp', u'$': u'Precipitation Probability'}

# The column names that are required.
colsReq = ['WindGust', 'Temperature', 'WindDirection', 'DewPoint',
           'ScreenRelativeHumidity', 'Pressure', 'PressureTendency', 'WindSpeed',
           'Temperature', 'Visibility', 'WeatherType']
colsNumericObs = ['WindGust', 'Temperature', 'DewPoint', 'Visibility',
               'ScreenRelativeHumidity', 'Pressure', 'WindSpeed', 'WeatherType']
colsNumericFcs = ['WindGust', 'Temperature', 'ScreenRelativeHumidity',
                  'WindSpeed', 'WeatherType', 'FeelsLikeTemperature',
                  'PrecipitationProbability', 'MaxUVIndex']
colsString = ['WindDirection', 'PressureTendency']

VisibilityCodes = {'UN': {'name': 'Unknown', 'min': None, 'max': None},
                   'VP': {'name': 'Very Poor', 'min': 0, 'max': 1000},
                   'PO': {'name': 'Poor', 'min': 1000, 'max': 4000},
                   'MO': {'name': 'Moderate', 'min': 4000, 'max': 10000},
                   'GO': {'name': 'Good', 'min': 10000, 'max': 20000},
                   'VG': {'name': 'Very Good', 'min': 20000, 'max': 40000},
                   'EX': {'name': 'Excellent', 'min': 40000, 'max': 60000}}

directions =  {
               'N': 0, 'NNE': 22.5, 'NE': 45, 'ENE': 67.5,
               'E': 90, 'ESE': 112.5, 'SE': 135, 'SSE': 157.5,
               'S': 180, 'SSW': 202.5, 'SW': 225, 'WSW': 247.5,
               'W': 270, 'WNW': 292.5, 'NW': 315, 'NNW': 337.5,
               'nan': 999, 'Null': 999
               }


def dataURL(APIKey, feed='capabilities', time='all', product='wxobs'):
  if product == 'wxobs':
    return ('http://datapoint.metoffice.gov.uk/'
            'public/data/val/wxobs/{}/json/'
            '{}?res=hourly&key={}').format(time, feed, APIKey)
  elif product == 'wxfcs':
    return ('http://datapoint.metoffice.gov.uk/'
            'public/data/val/wxfcs/{}/json/'
            '{}?res=3hourly&key={}').format(time, feed, APIKey)
  elif product == 'maplayer':
    return ('http://datapoint.metoffice.gov.uk/'
            'public/data/layer/wxobs/all/json/capabilities'
            '?key={}').format(APIKey)
  else:
    raise ValueError('Product {} not understood.'.format(product))

def unpackURL(URL):
  response = urllib.request.urlopen(URL)
  content = response.read().decode('utf8')
  data = json.loads(content)
  return data

def getSiteNames(APIKey=''):
  URL = dataURL(APIKey, feed='sitelist')
  data = unpackURL(URL)
  Locations = data['Locations']['Location']
  Locations = pd.DataFrame(Locations)
  return Locations

def getSiteID(APIKey, name, Locations='NotSet'):
  if isinstance(Locations, str):
    if Locations == 'NotSet':
      Locations = getSiteNames(APIKey=APIKey)
  locationNames = list(Locations.name)
  if name in locationNames:
    ID = int(Locations[Locations.name == name].id)
    return ID
  else:
    print('No exact match for "{}". The 10 closest matches are:'.format(name))
    matches = process.extract(name, Locations.name, limit=10)
    for match in matches:
      print(match)
    return 0

def getDateTime(data, dayI=0, fieldname='DateTime', forecast=False, timezone='Europe/London'):
  tz = pytz.timezone(timezone)
  Now = datetime.now()
  date0 = datetime(Now.year, Now.month, Now.day, 00, 00)
  date0 = tz.localize(date0)
  if forecast:
    date0 = date0 + timedelta(days = dayI)
  else:
    date0 = date0 - timedelta(days = 1-dayI)

  dts = [0]*len(data.index)
  for index, row in data.iterrows():
    minuteOfDay = int(row['MinuteOfDay'])
    dts[index] = date0 + timedelta(minutes=minuteOfDay)

  data[fieldname] = dts
  return data

def mostRecentTime(APIKey=''):
  URL_ = dataURL(APIKey, feed='capabilities')
  response = urllib.request.urlopen(URL_)
  content = response.read().decode('utf8')
  data = json.loads(content)
  tss = data['Resource']['TimeSteps']['TS']
  return tss[-1]

def getLayerURL(APIKey, layerName, dataType='Observation'):
  infourl = dataURL(APIKey, product='maplayer')

  if dataType in ['Observation', 'Observations']:
    infourl =('http://datapoint.metoffice.gov.uk/public/data/layer/wxobs/'
              'all/json/capabilities?key={}').format(APIKey)
  elif dataType == 'Forecast':
    infourl =('http://datapoint.metoffice.gov.uk/public/data/layer/wxfcs/'
              'all/json/capabilities?key={}').format(APIKey)
  else:
    raise ValueError('datatype {} not understood.'.format(dataType))

  response = urllib.request.urlopen(infourl)
  content = response.read().decode('utf8')
  data = json.loads(content)
  data = data['Layers']
  baseURL = data['BaseUrl']['$']

  gotIt = False
  names = []
  for l in data['Layer']:
    name = l['@displayName']
    names.append(name)
    if name == layerName:
      data = l['Service']
      gotIt = True
      break

  if not gotIt:
    raise ValueError('layerName {} cannot be found. Available layerNames are {}.'.format(layerName, ', '.join(names)))

  if dataType in ['Observation', 'Observations']:
    times = data['Times']['Time']
    URLs = dict()
    defaultTime = None
    for t in times:
      URLs[t] = baseURL.format(LayerName=data['LayerName'],
                               ImageFormat=data['ImageFormat'],
                               Time=t,
                               key=APIKey)

  elif dataType in ['Forecast']:
    defaultTime = data['Timesteps']['@defaultTime']
    times = data['Timesteps']['Timestep']
    URLs = dict()
    for t in times:
      URLs[t] = baseURL.format(LayerName=data['LayerName'],
                               ImageFormat=data['ImageFormat'],
                               DefaultTime=defaultTime,
                               Timestep=t,
                               key=APIKey)
  return URLs, defaultTime


def logLayer(URLs, layerName,  dataType='Observation',
             baseTime=None, timezone='Europe/London'):

  tz = pytz.timezone(timezone)

  keys = list(URLs.keys())
  if dataType in ['Observation', 'Observations']:
    dts = [tz.localize(datetime.strptime(dt, '%Y-%m-%dT%H:%M:%S')) for dt in keys]
    hours_ahead = np.zeros_like(dts)
    baset = None
  elif dataType == 'Forecast':
    hours_ahead = [int(x) for x in keys]
    baset = tz.localize(datetime.strptime(baseTime, '%Y-%m-%dT%H:%M:%S'))
    dts = [baset + timedelta(hours = x) for x in hours_ahead]
  else:
    raise ValueError('Not ready yet.')

  for di, key in enumerate(keys):
    dt = dts[di]
    ha = hours_ahead[di]
    url = URLs[key]
    # Add the row if it does not already exist. Do this manually so that we don't
    # have to deal with downloading files more than once.
    try:
      obj = met_layer.objects.get(layerName=layerName,
                                  dataType=dataType,
                                  timestamp=dt,
                                  hours_ahead=ha)
    except met_layer.DoesNotExist:
      # Get the image.
      response = urllib.request.urlopen(url)
      content = response.read()
      img_temp = NamedTemporaryFile(delete=True)
      img_temp.write(content)
      img_temp.flush()
      if baset is None:
        baset = dt
      obj = met_layer(layerName=layerName,
                      dataType=dataType,
                      timestamp=dt,
                      basetime=baset,
                      hours_ahead=ha)
      obj.save()
      print('{}.png'.format(obj))
      obj.image.save('{}.png'.format(obj), File(img_temp))
      obj.save()

def removeOldLayers(layerName, dataType='Observation'):
  if dataType in ['Observation', 'Observations']:
    pass # For now. Should probably remove older than 1 day, for example.
  elif dataType == 'Forecast':
    # Remove all but the most recent basetime.
    mls = met_layer.objects.filter(layerName=layerName,
                                   dataType='Forecast')
    mls = mls.order_by('basetime')
    mostrecent = mls[len(mls)-1].basetime
    mls.exclude(basetime=mostrecent).delete()




def downloadMet(APIKey, LocationID, time='all', product='wxobs'): #, timeSteps=[]):
  if product == 'wxfcs':
    forecast = True
  else:
    forecast = False
  if isinstance(LocationID, str):
    if LocationID != 'all':
      LocationID = getSiteID(APIKey, LocationID, Locations=Locations)
  URL = dataURL(APIKey, feed=LocationID, product=product) # Specifying
                                         # the timestamp doesn't
                                         # seem to have any effect.
  response = urllib.request.urlopen(URL)
  content = response.read().decode('utf8')
  data = json.loads(content)
  data = data['SiteRep']['DV']['Location']# DV for the data, Wx for meta data.
  if not isinstance(data, list):
    data = [data]
  first = True
  for stationdata in data:
    stat_id = int(stationdata['i'])
    if (LocationID != 'all') and (stat_id != LocationID):
      raise ValueError('StationID is not as expected.')
    stationdata = stationdata['Period']
    if isinstance(stationdata, dict):
      stationdata = [stationdata]
    for dayi in range(len(stationdata)):
      #print(stationdata[dayi])
      daydata = pd.DataFrame(stationdata[dayi]['Rep'])
      colNames = list(daydata)
      renames = {}
      for colName in colNames:
        renames[colName] = Params[colName]['$'].replace(' ', '')
      daydata = daydata.rename(columns=renames)
      daydata = getDateTime(daydata, dayI=dayi, forecast=forecast)
      if dayi == 0:
        stationdata_ = daydata
      else:
        stationdata_ = stationdata_.append(daydata)
    stationdata = stationdata_
    # Add the location ID to the dataframe.
    stationdata['LocationID'] = stat_id
    # Remove unwanted time stamps.
    if time != 'all':
      stationdata = stationdata[stationdata['DateTime'] == time]
      stationdata = stationdata.copy()
    # Add any missing fields as 'Null'.
    if not forecast:
      cols = list(stationdata)
      for colReq in colsReq:
        if colReq not in cols:
          if colReq in colsNumericObs:
            stationdata[colReq] = np.nan
          else:
            stationdata[colReq] = 'Null'
    if first:
      data_all = stationdata
      first = False
    else:
      data_all = data_all.append(stationdata)
    # Convert all appropriate columns to numeric.
    # For some reason they are all 'object' type.
    if forecast:
      data_all[colsNumericFcs] = data_all[colsNumericFcs].apply(pd.to_numeric)
      data_all['VisibilityName'] = data_all['Visibility'].apply(lambda x: VisibilityCodes[x]['name'])
      data_all['VisibilityMin'] = data_all['Visibility'].apply(lambda x: VisibilityCodes[x]['min'])
      data_all['VisibilityMax'] = data_all['Visibility'].apply(lambda x: VisibilityCodes[x]['max'])
    else:
      data_all[colsNumericObs] = data_all[colsNumericObs].apply(pd.to_numeric)
    data_all['WindDirection_Angle'] = data_all['WindDirection'].apply(
                                                  lambda x: directions[str(x)])
                                       # The str(x) is to catch NaN directions.

  return data_all

def logMet(data):
  colNames = list(data)
  for index, row in data.iterrows():
    # Replace nan values with None
    for col in colNames:
      v = row[col]
      if (isinstance(v, float)) and (np.isnan(v)):
        row[col] = None

    # Add the row, or update it if it already exists.
    obj, created = met_obs.objects.update_or_create(
                            location_id=row.LocationID,
                            timestamp=row.DateTime,
                            defaults={'weather_type': row.WeatherType,
                                      'pressure': row.Pressure,
                                      'pressure_tendency': row.PressureTendency,
                                      'wind_speed': row.WindSpeed,
                                      'wind_gust': row.WindGust,
                                      'wind_direction': row.WindDirection,
                                      'temperature': row.Temperature,
                                      'dew_point': row.DewPoint,
                                      'visibility': row.Visibility})
    if created:
      print('Created new item')
    else:
      print('Updated item')
    print(obj)

def logMetLocations(data_locs):
  # Get the latest observations for all locations.
  # get the most recent time
  mrt = mostRecentTime(APIKey=APIKey)
  ObsAll = downloadMet(APIKey, 'all', time=mrt)
  colNames = list(ObsAll)

  for index, row in data_locs.iterrows():
    loc_id = int(row.id)
    # Get the observations for this location.
    obs = ObsAll.loc[ObsAll['LocationID'] == loc_id, :]
    if len(obs.index) == 0:
      print('No observations for station {}: {}.'.format(loc_id, row['name']))
      continue
    obs = obs.iloc[0]
    # Replace nan values with None
    for col in colNames:
      v = obs[col]
      if (isinstance(v, float)) and (np.isnan(v)):
        obs[col] = None
    # Add the row, or update it if it already exists.
    obj, created = met_locations.objects.update_or_create(
                            location_id=row.id,
                            defaults={'name': row['name'],
                                      'region': row.region,
                                      'unitary_authority': row.unitaryAuthArea,
                                      'observation_source': row.obsSource,
                                      'national_park': row.nationalPark,
                                      'latitude': row.latitude,
                                      'longitude': row.longitude,
                                      'elevation': row.elevation,
                                      'timestamp': obs.DateTime,
                                      'weather_type': obs.WeatherType,
                                      'pressure': obs.Pressure,
                                      'pressure_tendency': obs.PressureTendency,
                                      'wind_speed': obs.WindSpeed,
                                      'wind_gust': obs.WindGust,
                                      'wind_direction': obs.WindDirection,
                                      'temperature': obs.Temperature,
                                      'dew_point': obs.DewPoint,
                                      'visibility': obs.Visibility})
    if created:
      print('Created new item')
    else:
      print('Updated item')
    print(obj)









# Get the site details.
#Locations = getSiteNames(APIKey)

if __name__ == '__main__':

  # Get the APIKey
  APIKey = tools.getSecret('MetOffice')


  parser = argparse.ArgumentParser(description="Extracts data from the met office datapoint API service")
  parser.add_argument('-m', metavar='mode',
                      type=str, nargs='?', default='observations',
                      choices=['observations','forecasts', 'locations', 'maplayer'],
                      help=("The mode of operation. What sort of data "
                            "is being collected. One of either 'observations', "
                            "'forecasts', or 'locations', 'maplayer'. Default 'observations'."))

  args = parser.parse_args()
  Locations = getSiteNames(APIKey=APIKey)
  #print(Locations)

  if args.m == 'observations':
    # Get the met data.
    data = downloadMet(APIKey, 'Edinburgh/Gogarbank')
    logMet(data)
    data = downloadMet(APIKey, 'Eskdalemuir')
    logMet(data)
    data = downloadMet(APIKey, 'Fair Isle')
    logMet(data)
    data = downloadMet(APIKey, 'Dundrennan')
    logMet(data)
  elif args.m == 'forecasts':
    # Get the met data.
    data = downloadMet(APIKey, 'Edinburgh/Gogarbank', product='wxfcs')
    print(data)
  elif args.m == 'locations':
    logMetLocations(Locations)
  elif args.m == 'maplayer':
    # Observations: Lightning, SatelliteIR, SatelliteVis, Rainfall
    URLs, baseT = getLayerURL(APIKey, 'Rainfall', dataType='Observation')
    logLayer(URLs, 'Rainfall', dataType='Observation')
    URLs, baseT = getLayerURL(APIKey, 'Rainfall', dataType='Forecast')
    logLayer(URLs, 'Rainfall', dataType='Forecast', baseTime=baseT)
    removeOldLayers('Rainfall', dataType='Forecast')

    #logMetLocations(Locations, APIKey=APIKey)
  else:
    raise ValueError("Mode '{}' is not understood.".format(args.mode))

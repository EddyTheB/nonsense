from django.conf.urls import url
from . import views

urlpatterns = [
  url(r'^$', views.index, name='meteorology'),
  url(r'^my_obs/', views.my_obs, name='meteorology'),
  url(r'^layers/([^/]*)', views.displaymaplayer, name='meteorology'),
  url(r'^([^/]*)', views.singlesite, name='meteorology'),
]

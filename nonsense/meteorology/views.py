from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse

from os import path

import pandas as pd
import numpy as np
import json
from datetime import datetime, timedelta

from bokeh.plotting import figure, ColumnDataSource
from bokeh.resources import CDN
from bokeh.embed import components
from bokeh.layouts import gridplot
from bokeh.models import DatetimeTickFormatter, Range1d, HoverTool, LinearAxis
from bokeh.models.glyphs import ImageURL, Text

from pages.models import Page
from meteorology.models import met_locations, met_layer, my_obs

# Import my own functions. I really want to import these from my tools directory
# but I can't seem to make that work!
import tools
import logMetObs


# https://www.metoffice.gov.uk/datapoint/support/documentation/code-definitions
weathericonsbase = "/static/weather_icons/png/"
weathericonsbase_ = "/weather_icons/png/"
weathericons = {
                0: {'url': 'wi-night-clear.png', 'desc': 'Clear night'},
                1: {'url': 'wi-day-sunny.png', 'desc': 'Sunny day'},
                2: {'url': 'wi-night-partly-cloudy.png', 'desc': 'Partly cloudy (night)'},
                3: {'url': 'wi-day-sunny-overcast.png', 'desc': 'Partly cloudy (day)'},
                #4: Not used
                5: {'url': 'wi-fog.png', 'desc': 'Mist'},
                6: {'url': 'wi-fog.png', 'desc': 'Fog'},
                7: {'url': 'wi-cloudy.png', 'desc': 'Cloudy'},
                8: {'url': 'wi-cloud.png', 'desc': 'Overcast'},
                9: {'url': 'wi-night-sprinkle.png', 'desc': 'Light rain shower (night)'},
               10: {'url': 'wi-day-sprinkle.png', 'desc': 'Light rain shower (day)'},
               11: {'url': 'wi-sprinkle.png', 'desc': 'Drizzle'},
               12: {'url': 'wi-sprinkle.png', 'desc': 'Light rain'},
               13: {'url': 'wi-night-rain.png',  'desc': 'Heavy rain shower (night)'},
               14: {'url': 'wi-day-rain.png', 'desc': 'Heavy rain shower (day)'},
               15: {'url': 'wi-rain.png', 'desc': 'Heavy rain'},
               16: {'url': 'wi-night-sleet.png', 'desc': 'Sleet shower (night)'},
               17: {'url': 'wi-day-sleet.png', 'desc': 'Sleet shower (day)'},
               18: {'url': 'wi-sleet.png', 'desc': 'Sleet'},
               19: {'url': 'wi-night-hail.png',  'desc': 'Hail shower (night)'},
               20: {'url': 'wi-day-hail.png', 'desc': 'Hail shower (day)'},
               21: {'url': 'wi-hail.png', 'desc': 'Hail'},
               22: {'url': 'wi-night-snow.png', 'desc': 'Light snow shower (night)'},
               23: {'url': 'wi-day-snow.png', 'desc': 'Light snow shower (day)'},
               24: {'url': 'wi-snow.png', 'desc': 'Light snow'},
               25: {'url': 'wi-night-snow.png', 'desc': 'Heavy snow shower (night)'},
               26: {'url': 'wi-day-snow.png', 'desc': 'Heavy snow shower (day)'},
               27: {'url': 'wi-snow.png', 'desc': 'Heavy snow'},
               28: {'url': 'wi-night-thunderstorm.png', 'desc': 'Thunder shower (night)'},
               29: {'url': 'wi-day-thunderstorm.png', 'desc': 'Thunder shower (day)'},
               30: {'url': 'wi-thunderstorm.png', 'desc': 'Thunder'}
                }
weatherwinddir = path.join(weathericonsbase, 'wi-wind-deg.png')


# Create your views here.

def index(request):

  data = met_locations.objects.all()
  data = pd.DataFrame.from_records(data.values())
  data = data[np.isfinite(data['weather_type'])].copy()

  longitudes = list(data['longitude'])
  latitudes = list(data['latitude'])
  loncentre = np.mean([min(longitudes), max(longitudes)])
  latcentre = np.mean([min(latitudes), max(latitudes)])
  names = json.dumps(list(data['name']))
  ids = json.dumps(list(data['location_id']))

  # Specify the icon locations.
  data['weathericons'] = data['weather_type'].apply(lambda x: path.join(weathericonsbase, weathericons[x]['url']))
  icons = json.dumps(list(data['weathericons']))

  context = {
    'page_list': Page.objects.all(),
    'longitudes': longitudes,
    'latitudes': latitudes,
    'loncentre': loncentre,
    'latcentre': latcentre,
    'names': names,
    'ids': ids,
    'icons': icons,
    'test_str': ids,
    }
  return render(request, "meteorology/index.html", context)


def displaymaplayer(request, layername):
  teststr = ''


  # Get images within the last few hours.
  time_threshold = datetime.now() - timedelta(hours=6)
  # Get observation images first.
  mls = met_layer.objects.filter(layerName=layername,
                                 dataType='Observation',
                                 timestamp__gt=time_threshold)
  mls = mls.order_by('timestamp')
  # Get the timestamps as strings
  dts = [datetime.strftime(x.timestamp, '%Y%m%d%H%M%S') for x in mls]
  # And get forecast images that are for later than the most recent observation.
  dtsLast = mls[len(mls)-1].timestamp
  mlsf = met_layer.objects.filter(layerName=layername,
                                  dataType='Forecast',
                                  timestamp__gt=dtsLast)
  mlsf = mlsf.order_by('timestamp')

  # Get the timestamps as strings
  dts = [datetime.strftime(x.timestamp, '%Y%m%d%H%M%S') for x in mls]
  dtsfcs = [datetime.strftime(x.timestamp, '%Y%m%d%H%M%S') for x in mlsf]
  dts.extend(dtsfcs)
  # Set the default conditions, most recent hour
  dti = len(mls)-1
  formdetails = {}
  formdetails['dtlast'] = dts[dti-1]
  formdetails['dtnext'] = dts[dti+1]
  formdetails['dtlastdisable'] = ''
  formdetails['dtnextdisable'] = ''

  # See if the dtaetime has been specified by GET
  if request.method == 'GET':
    GETResults = request.GET
    try:
      dt = GETResults['dt']
      dti = dts.index(dt)
      if dti == len(dts)-1:
        formdetails['dtlast'] = dts[dti-1]
        formdetails['dtnext'] = '---'
        formdetails['dtlastdisable'] = ''
        formdetails['dtnextdisable'] = 'disabled'
      elif dti == 0:
        formdetails['dtlast'] = '---'
        formdetails['dtnext'] = dts[dti+1]
        formdetails['dtlastdisable'] = 'disabled'
        formdetails['dtnextdisable'] = ''
      else:
        formdetails['dtlast'] = dts[dti-1]
        formdetails['dtnext'] = dts[dti+1]
        formdetails['dtlastdisable'] = ''
        formdetails['dtnextdisable'] = ''
    except:
      pass

  # Select the layer, send to template.
  if dti < len(mls):
    # Observation
    layerurl = mls[dti].image.url
    layerdesc = str(mls[dti])
  else:
    # Forecast
    dtii = dti - len(mls)
    layerurl = mlsf[dtii].image.url
    layerdesc = str(mlsf[dtii])

  context = {
    'page_list': Page.objects.all(),
    'layerdesc': layerdesc,
    'layerurl': layerurl,
    'teststr': teststr,
    'formdetails': formdetails
    }
  return render(request, "meteorology/maplayer.html", context)


#def my_obs(request):
#  teststr = 'Test Test!'
#
#  dateto = datetime.now()
#  datefrom = dateto - timedelta(days=7)
#  # Extract the data from the database
#  model = getattr(mods, 'my_obs')
#
#
#
#  context = {
#    'page_list': Page.objects.all(),
#    'test_str': teststr,
#    'obs_script': '',#scriptO,
#    'obs_div': ''#divO
#    }
#  return render(request, "meteorology/my_obs.html", context)



def singlesite(request, sitenum):
  st = get_object_or_404(met_locations, location_id=sitenum)
  teststr = ''
  st_id = st.location_id
  #Download the latest observations from the api.
  APIKey = tools.getSecret('MetOffice', homeDir='/home/pi')
  metobs = logMetObs.downloadMet(APIKey, st_id)
  metobs['DateTimeStr'] = metobs['DateTime'].apply(lambda x: '{}'.format(x))
  metobs['Visibility'] /= 1000
  metobs['WeatherIcons'] = metobs['WeatherType'].apply(lambda x: path.join(weathericonsbase, weathericons[x]['url']))
  metobs['WeatherBeaufort'] = metobs['WindSpeed'].apply(lambda x: path.join(weathericonsbase, 'wi-wind-beaufort-{}.png'.format(tools.beaufort(int(x)))))
  metobs['WeatherDesc'] = metobs['WeatherType'].apply(lambda x: weathericons[x]['desc'])
  #metobs['diricons'] = weatherwinddir
  metobs['dirtext'] = u'V'
  metobs['dirVdirection'] = metobs['WindDirection_Angle'] * -1 # + 180
  metobs['winddiry'] = 1.
  metobs3 = metobs[metobs['DateTime'].dt.hour % 3 == 0].copy()
  metobs6 = metobs[metobs['DateTime'].dt.hour % 6 == 0].copy()
  teststr = ', '.join([str(x) for x in list(metobs3['DateTime'])])

  metfcs3 = logMetObs.downloadMet(APIKey, st_id, product='wxfcs')
  metfcs3['DateTimeStr'] = metfcs3['DateTime'].apply(lambda x: '{}'.format(x))
  metfcs3['VisibilityMin'] /= 1000
  metfcs3['VisibilityMax'] /= 1000
  metfcs3['WeatherIcons'] = metfcs3['WeatherType'].apply(lambda x: path.join(weathericonsbase, weathericons[x]['url']))
  metfcs3['WeatherBeaufort'] = metfcs3['WindSpeed'].apply(lambda x: path.join(weathericonsbase, 'wi-wind-beaufort-{}.png'.format(tools.beaufort(int(x)))))
  metfcs3['WeatherDesc'] = metfcs3['WeatherType'].apply(lambda x: weathericons[x]['desc'])
  #metfcs['diricons'] = weatherwinddir
  metfcs3['dirtext'] = u'V'
  metfcs3['dirVdirection'] = metfcs3['WindDirection_Angle'] * -1 # + 180

  metfcs6 = metfcs3[metfcs3['DateTime'].dt.hour % 6 == 0].copy()
  #metobs['three'] = 3.
  #metobs['four'] = 4.
  #colnames = list(metobs)
  #teststr = ', '.join([str(x) for x in list(metfcs['WindDirection_Angle'])])

  # Decide some y ranges, only neccesary for temperature and wind speed because
  # these will have some additional symbols added.
  minT = min([metfcs3['FeelsLikeTemperature'].min(), metobs['DewPoint'].min()])
  maxT = max([metfcs3['Temperature'].max(), metobs['Temperature'].max()])
  if minT > 0:
    minT = 0
  TRange = maxT-minT
  metobs6['aboveTemp'] = maxT + TRange/10
  metfcs6['aboveTemp'] = maxT + TRange/10
  maxT += TRange/5
  minT -= TRange/5
  minW = min([metfcs3['WindGust'].min(), metobs['WindGust'].min(),
              metfcs3['WindSpeed'].min(), metobs['WindSpeed'].min()])
  maxW = max([metfcs3['WindGust'].max(), metobs['WindGust'].max(),
              metfcs3['WindSpeed'].max(), metobs['WindSpeed'].max()])
  if minW > 0:
    minW = 0
  WRange = maxW-minW
  metobs6['aboveWind'] = maxW + 2*WRange/10
  metfcs6['aboveWind'] = maxW + 2*WRange/10
  metobs6['aboveWind2'] = maxW + WRange/10
  metfcs6['aboveWind2'] = maxW + WRange/10
  maxW += WRange/3
  minW -= WRange/5
  #teststr = '{} - {}'.format(minT, maxT)
  dataobs = ColumnDataSource(metobs)
  #dataobs3 = ColumnDataSource(metobs3)
  datafcs3 = ColumnDataSource(metfcs3)
  dataobs6 = ColumnDataSource(metobs6)
  datafcs6 = ColumnDataSource(metfcs6)

  data = dataobs

  ## Observations and Forecast Plots
  # The temperature plot
  plotT = figure(x_axis_type='datetime',
                plot_width=760, plot_height=300,
                tools="xpan,xwheel_zoom,reset",
                y_range=Range1d(minT, maxT))
  # Plot the temperatures.
  plotT.line('DateTime', 'Temperature', source=dataobs, line_width=2,
             color='firebrick', line_dash='dashed', legend='Observed Temperature ')
  plotT.line('DateTime', 'DewPoint', source=dataobs, line_width=2,
             color='blue', line_dash='dashed',  legend='Observed Dew Point ')
  plotT.line('DateTime', 'Temperature', source=datafcs3, line_width=2,
             color='firebrick', legend='Forecast Temperature ')
  plotT.line('DateTime', 'FeelsLikeTemperature', source=datafcs3,
             line_width=2, color='green', legend='Forecast Feels Like Temperature ')
  # Plot the weather symbols every 6 hours.
  glyph = ImageURL(x='DateTime', y='aboveTemp', url='WeatherIcons', anchor='center')
  plotT.add_glyph(dataobs6, glyph)
  plotT.add_glyph(datafcs6, glyph)
  plotT.yaxis.axis_label = 'Degrees C'
  plotT.legend.location = "bottom_left"
  plotT.legend.orientation = "horizontal"
  plotT.legend.background_fill_alpha = 0.3
  plotT.add_tools(HoverTool())
  hover = plotT.select(dict(type=HoverTool))
  hoverToolTips = []
  hoverToolTips.append(('Time', "@DateTimeStr"))
  hoverToolTips.append(('Weather', "@WeatherDesc"))
  hoverToolTips.append(('Temperature (C)', "@Temperature"))
  hoverToolTips.append(('Dew Point (C)', "@DewPoint"))
  hoverToolTips.append(('Feels Like (C)', "@FeelsLikeTemperature"))
  hover.tooltips = hoverToolTips

  # Wind speed
  plotW = figure(x_axis_type='datetime',
                plot_width=760, plot_height=300,
                tools="xpan,xwheel_zoom,reset",
                x_range=plotT.x_range,
                y_range=Range1d(minW, maxW))
  plotW.line('DateTime', 'WindSpeed', source=dataobs, line_width=2,
             line_dash='dashed', color='navy', legend='Observed Wind Speed ')
  plotW.line('DateTime', 'WindSpeed', source=datafcs3, line_width=2,
             color='navy', legend='Forecast Wind Speed ')
  plotW.circle('DateTime', 'WindGust', source=dataobs, color='black', legend='Wind Gust ')
  plotW.circle('DateTime', 'WindGust', source=datafcs3, color='black', legend='Wind Gust ')
  glyph = Text(x='DateTime', y='aboveWind', text="dirtext", text_align='center',
               text_baseline='middle', angle='dirVdirection', angle_units='deg')
  plotW.add_glyph(datafcs6, glyph)
  plotW.add_glyph(dataobs6, glyph)
  glyph = ImageURL(x='DateTime', y='aboveWind2', url='WeatherBeaufort', anchor='center')
  plotW.add_glyph(dataobs6, glyph)
  plotW.add_glyph(datafcs6, glyph)

  plotW.yaxis.axis_label = 'Wind Speed (mph)'
  plotW.legend.location = "bottom_left"
  plotW.legend.orientation = "horizontal"
  plotW.legend.background_fill_alpha = 0.3
  plotW.add_tools(HoverTool())
  hover = plotW.select(dict(type=HoverTool))
  hoverToolTips = []
  hoverToolTips.append(('Time', "@DateTimeStr"))
  hoverToolTips.append(('Wind Speed (mph)', "@WindSpeed"))
  hoverToolTips.append(('Wind DIrection', "@WindDirection"))
  hoverToolTips.append(('Gust Speed (mph)', "@WindGust"))
  hover.tooltips = hoverToolTips

  plotsBoth = gridplot([[plotT], [plotW]])

  ## Observations only plots
  # The pressure plot
  plotP = figure(x_axis_type='datetime',
                plot_width=760, plot_height=300,
                tools="xpan,xwheel_zoom,reset")
  plotP.line('DateTime', 'Pressure', source=data, line_width=2, color='navy', legend='Pressure ')
  plotP.yaxis.axis_label = 'Pressure (hPa)'
  plotP.legend.location = "bottom_left"
  plotP.add_tools(HoverTool())
  hover = plotP.select(dict(type=HoverTool))
  hoverToolTips = []
  hoverToolTips.append(('Time', "@DateTimeStr"))
  hoverToolTips.append(('Pressure (hPa)', "@Pressure"))
  hoverToolTips.append(('Pressure Tendency', "@PressureTendency"))
  hover.tooltips = hoverToolTips
  # Humidity
  plotH = figure(x_axis_type='datetime',
                plot_width=760, plot_height=300,
                tools="xpan,xwheel_zoom,reset",
                x_range=plotP.x_range)
  plotH.line('DateTime', 'ScreenRelativeHumidity', source=data, line_width=2, color='navy', legend='Relative Humidity ')
  plotH.yaxis.axis_label = 'Relative Humidity (%)'
  plotH.legend.location = "bottom_left"
  plotH.add_tools(HoverTool())
  hover = plotH.select(dict(type=HoverTool))
  hoverToolTips = []
  hoverToolTips.append(('Time', "@DateTimeStr"))
  hoverToolTips.append(('Relative Humidity (%)', "@ScreenRelativeHumidity"))
  hover.tooltips = hoverToolTips
  # Visibility
  plotV = figure(x_axis_type='datetime',
                plot_width=760, plot_height=300,
                tools="xpan,xwheel_zoom,reset",
                x_range=plotP.x_range)
  plotV.line('DateTime', 'Visibility', source=data, line_width=2, color='navy', legend='Visibility ')
  plotV.yaxis.axis_label = 'Visibility (km)'
  plotV.legend.location = "bottom_left"
  plotV.add_tools(HoverTool())
  hover = plotV.select(dict(type=HoverTool))
  hoverToolTips = []
  hoverToolTips.append(('Time', "@DateTimeStr"))
  hoverToolTips.append(('Visibility (km)', "@Visibility"))
  hover.tooltips = hoverToolTips

  plotsObs = gridplot([[plotP], [plotH], [plotV]])


  scriptB, divB = components(plotsBoth, CDN)
  scriptO, divO = components(plotsObs, CDN)


  context = {
    'site': st,
    'page_list': Page.objects.all(),
    'test_str': '',#teststr,
    'both_script': scriptB,
    'both_div': divB,
    'obs_script': scriptO,
    'obs_div': divO
    }
  return render(request, "meteorology/singlesite.html", context)


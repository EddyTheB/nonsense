from django.contrib import admin

# Register your models here.
from .models import met_locations, met_obs, met_layer, my_obs

admin.site.register(met_layer)
admin.site.register(met_locations)
admin.site.register(met_obs)
admin.site.register(my_obs)

from django.db import models
from django.utils import timezone

# Create your models here.
class met_layer(models.Model):
  dTChoices = (('Forecast', 'Forecast'),
               ('Observation', 'Observation'),)
  lTChoices = (('Lightning', 'Lightning'),
               ('SatelliteIR', 'SatelliteIR'),
               ('SatelliteVis', 'SatelliteVis'),
               ('Rainfall', 'Rainfall'))
  
  layerName = models.CharField(max_length=12, choices=lTChoices)
  dataType = models.CharField(max_length=12, choices=dTChoices)
  timestamp = models.DateTimeField()
  basetime = models.DateTimeField()
  hours_ahead = models.SmallIntegerField()
  image = models.ImageField(null=True, upload_to='layer')
  added = models.DateTimeField(editable=False)
  updated = models.DateTimeField()

  def save(self, *args, **kwargs):
    ''' On save, update timestamps '''
    if not self.id:
      self.added = timezone.now()
    self.updated = timezone.now()
    return super(met_layer, self).save(*args, **kwargs)
    
  def __str__(self):
    return '{} - {} - {} - T{}'.format(self.layerName, self.dataType, self.timestamp, self.hours_ahead)

class met_locations(models.Model):
  location_id = models.BigIntegerField()
  name = models.CharField(max_length=40)
  region = models.CharField(max_length=4, null=True)
  unitary_authority = models.CharField(max_length=30)
  observation_source = models.CharField(max_length=10, null=True)
  national_park = models.CharField(max_length=50, null=True)
  latitude = models.FloatField()
  longitude = models.FloatField()
  elevation = models.FloatField()
  # Add fields to store the most recent observations for each site.
  timestamp = models.DateTimeField()
  weather_type = models.SmallIntegerField(null=True)
  pressure = models.SmallIntegerField(null=True)
  pressure_tendency = models.CharField(max_length=4, blank=True)
  wind_speed = models.FloatField(null=True)
  wind_gust = models.FloatField(null=True)
  wind_direction = models.CharField(max_length=4, null=True)
  temperature = models.FloatField(null=True)
  dew_point = models.FloatField(null=True)
  visibility = models.BigIntegerField(null=True)
  added = models.DateTimeField(editable=False)
  updated = models.DateTimeField()
  
  def save(self, *args, **kwargs):
    ''' On save, update timestamps '''
    if not self.id:
      self.added = timezone.now()
    self.updated = timezone.now()
    return super(met_locations, self).save(*args, **kwargs)

  def __str__(self):
    return '{}: {}'.format(self.location_id, self.name)


class my_obs(models.Model):
  timestamp = models.DateTimeField()
  pressure = models.SmallIntegerField(null=True)
  wind_speed = models.FloatField(null=True)
  wind_speed = models.FloatField(null=True)
  wind_direction = models.CharField(max_length=4, blank=True, default='---')
  temperature = models.FloatField(null=True)
  humidity = models.FloatField(null=True)
  precipitation = models.FloatField(null=True)
  extra_field1 = models.FloatField(null=True)
  extra_field2 = models.FloatField(null=True)
  extra_field3 = models.FloatField(null=True)

  def __str__(self):
    return '{}'.format(self.timestamp)

class met_obs(models.Model):
  location_id = models.BigIntegerField()
  timestamp = models.DateTimeField()
  weather_type = models.SmallIntegerField(null=True)
  pressure = models.SmallIntegerField(null=True)
  pressure_tendency = models.CharField(max_length=4, blank=True)
  wind_speed = models.FloatField(null=True)
  wind_gust = models.FloatField(null=True)
  wind_direction = models.CharField(max_length=4, blank=True)
  temperature = models.FloatField(null=True)
  dew_point = models.FloatField(null=True)
  visibility = models.BigIntegerField(null=True)
  added = models.DateTimeField(editable=False)
  updated = models.DateTimeField()

  def save(self, *args, **kwargs):
    ''' On save, update timestamps '''
    if not self.id:
      self.added = timezone.now()
    self.updated = timezone.now()
    return super(met_obs, self).save(*args, **kwargs)

  def __str__(self):
    return '{} {}'.format(self.location_id, self.timestamp)

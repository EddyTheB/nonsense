from django.apps import AppConfig


class MeteorologyConfig(AppConfig):
    name = 'meteorology'

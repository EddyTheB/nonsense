from os import path
import json
import datetime
import numpy as np
homeDir = path.expanduser("~")


def beaufort(v):
  """
  Converts mph to beaufort scale
  """
  if v < 1:
    return 0
  elif v <= 3:
    return 1
  elif v <= 7:
    return 2
  elif v <= 12:
    return 3
  elif v <= 18:
    return 4
  elif v <= 24:
    return 5
  elif v <= 31:
    return 6
  elif v <= 38:
    return 7
  elif v <= 46:
    return 8
  elif v <= 54:
    return 9
  elif v <= 63:
    return 10
  elif v <= 72:
    return 11
  else:
    return 12

def linearRegND(X, y):
  X = np.matrix(X)
  Y = np.matrix(y)
  # Make sure the order is correct. Y should be a column vector.
  ya, yb = np.shape(Y)
  if ya == 1:
    Y = Y.transpose()
  elif yb == 1:
    pass
  else:
    raise ValueError('y should be a vector.')
  xa, xb = np.shape(X)
  if xb == len(y):
    X = X.transpose()
  elif xa == len(y):
    pass
  else:
    raise ValueError('x should be a matrix with the same number of rows as there are elements in vector y.')

  XT = np.transpose(X)
  A = np.dot(XT, X)
  b = np.dot(XT, Y)
  w = np.linalg.solve(A, b)
  return w

def linearReg1D(x, y):
  """
  Performslinear regression to find the best fittig line through the points
  contained in arrays x and y, such that the sum of the squares of the
  difference between those points and a line given by y = mX +c is minimised.

  returns the parameters m and c.
  """

  x = np.array(x)
  y = np.array(y)
  xm = np.mean(x)
  ym = np.mean(y)

  xy = np.dot(x, y)
  xx = np.dot(x, x)

  D = xx - xm*x.sum()
  m = (xy -ym*x.sum())/D
  c = (ym*xx -xm*xy)/D
  return m, c

def RSquared(y, y_):
  # Make sure these are arrays, not matrices, so that multiplications are done elementwise.
  y = np.array(y)
  y_ = np.array(y_)
  SSres = np.sum((y - y_)**2)
  SStot = np.sum((y - np.mean(y))**2)
  return 1 - SSres/SStot

def getSecret(secretname, filename='~.keys/keys', homeDir=homeDir):
  filename = filename.replace('~', homeDir+'/')
  with open(filename) as json_data:
    d = json.load(json_data)
    try:
      secret = d[secretname]
    except KeyError:
      print('No secret named {}'.format(secretname))
      return 'None'
    return secret

def secondsToString(seconds, form='short'):
  td = datetime.timedelta(seconds=seconds)
  if form == 'short':
    return str(td)
  elif form == 'long':
    d = td.days
    s = td.seconds
    m, s = divmod(s, 60)
    h, m = divmod(m, 60)
    if m == 0:
      return '{} seconds'.format(s)
    elif h == 0:
      return '{} minutes and {} seconds'.format(m, s)
    elif d == 0:
      return '{} hours, {} minutes and {} seconds'.format(h, m, s)
    else:
      return '{} days, {}hours, {} minutes and {} seconds'.format(d, h, m, s)
  else:
    raise ValueError("Format '{}' is not understood.".format(form))


def numToLetter(N, ABC=u'ABCDEFGHIJKLMNOPQRSTUVWXYZ' ):
  """
  Converts a number in to a letter, or set of letters, based on the contents of
  ABC. If ABC contains the alphabet (which is the default behaviour) then for a
  number n this will result in the same string as the name of the nth column in
  a normal spreadsheet.

  INPUTS
  N    - integer - any number.
  OPTIONAL INPUTS
  ABC  - string  - any string of characters (or any other symbols I believe).
  OUTPUTS
  L    - string  - the number converted to a string.

  EXAMPLES
  numToLetter(1)
  u'A'
  numToLetter(26)
  u'Z'
  numToLetter(27)
  u'AA'
  numToLetter(345)
  u'MG'
  numToLetter(345, ABC='1234567890')
  u'345'
  """
  L = ''
  sA = len(ABC)
  div = int(N)
  while div > 0:
    R = int((div-1)%sA)
    L = ABC[R] + L
    div = int((div-R)/sA)
  return L


if __name__ == '__main__':
  # For testing.
  ss = secondsToString(4321)
  print(ss)
  sl = secondsToString(4321, form='long')
  print(sl)

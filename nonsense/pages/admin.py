from django.contrib import admin
from .models import Page, PageV2, PageV2Content

class PageAdmin(admin.ModelAdmin):
  list_display = ('title', 'update_date')
  ordering = ('title',)
  search_fields = ('title',)

class PageV2Admin(admin.ModelAdmin):
  list_display = ('title', 'permalink', 'update_date')
  ordering = ('title',)
  search_fields = ('title',)

# Register your models here.
admin.site.register(Page, PageAdmin)
admin.site.register(PageV2, PageV2Admin)
admin.site.register(PageV2Content)

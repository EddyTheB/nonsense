from django.db import models

# Create your models here.

class Page(models.Model):
  title = models.CharField(max_length=60)
  permalink = models.CharField(max_length=40, unique=True)
  update_date = models.DateTimeField('Last Updated')
  bodytext = models.TextField('Page Content', blank=True)

  def __str__(self):
    return self.title





class PageV2Content(models.Model):
  page = models.ForeignKey('PageV2', on_delete=models.CASCADE)
  bodytext = models.TextField('Page Content', blank=True)
  language = models.CharField(max_length=40, default='English')

  def __str__(self):
    return '{} - {}'.format(self.page.title, self.language)

class PageV2(models.Model):
  title = models.CharField(max_length=60)
  permalink = models.CharField(max_length=40, unique=True)
  update_date = models.DateTimeField('Last Updated')
  #parentPermalink = models.CharField(max_length=40, default='')
  includeOnMenu = models.BooleanField(default=True)
  # if includeOnMenu is True, then the page will appear on menus on the
  # parent page.

  def __str__(self):
    return self.title






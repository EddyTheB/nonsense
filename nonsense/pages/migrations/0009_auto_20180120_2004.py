# Generated by Django 2.0 on 2018-01-20 20:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0008_auto_20180120_1959'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datafeed',
            name='xScaling',
            field=models.FloatField(null=True),
        ),
        migrations.AlterField(
            model_name='datafeed',
            name='yScaling',
            field=models.FloatField(null=True),
        ),
    ]

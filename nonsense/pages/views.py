from django.shortcuts import render, get_object_or_404
#from django.http import HttpResponseRedirect
#from django.core.mail import send_mail, get_connection

#from bokeh.plotting import figure, ColumnDataSource
#from bokeh.resources import CDN
#from bokeh.embed import components
#from bokeh.models import DatetimeTickFormatter,Range1d,  WheelZoomTool, HoverTool

#from datetime import datetime, timedelta

#import numpy as np
#import pandas as pd
#from math import pi
#import importlib

#from .models import Page, DataFeed #internet_speed
from . import models as mods
#from .forms import ContactForm, internetSpeedForm



# Create your views here.
def index(request):
  return page(request, 'about')


def page(request, link):
  if link == '':
    link = 'about'
  link = '/' + link
  pg = get_object_or_404(mods.PageV2, permalink=link)
  #pg.id

  # Get all contents.
  pg_contents = mods.PageV2Content.objects.filter(page_id=pg.id)
  languagelist = []
  for pg_c in pg_contents:
    languagelist.append(pg_c.language)


  got_text = False
  if request.method == 'GET':
    GETResults = request.GET
    try:
      lg = GETResults['languageSelect']
      pg_text = get_object_or_404(mods.PageV2Content, page_id=pg.id, language=lg)
      text = pg_text.bodytext
      got_text = True
    except:
      pass
  if not got_text:
    # Try English
    try:
      pg_text = get_object_or_404(mods.PageV2Content, page_id=pg.id, language='English')
      text = pg_text.bodytext
      lg = 'English'
    except:
      # Take the first available
      pg_text = pg_contents[0]
      text = pg_text.bodytext
      lg = pg_text.language


  context = {
    'title': pg.title,
    'url': pg.permalink,
    'languagelist': languagelist,
    'language': lg,
    'bodytext': text,
    'last_updated': pg.update_date,
    'page_list': mods.Page.objects.all(),
    }
  return render(request, "pages/page.html", context)

"""
def contact(request):
  submitted = False
  if request.method == 'POST':
    form = ContactForm(request.POST)
    if form.is_valid():
      cd = form.cleaned_data
      con = get_connection('django.core.mail.backends.console.EmailBackend')
      send_mail(cd['subject'],
                cd['message'],
                cd.get('email', 'noreply@example.com'),
                ['siteowner@example.com'],
                connection=con)
      return HttpResponseRedirect('/contact?submitted=True')
  else:
    form = ContactForm()
    if 'submitted' in request.GET:
      submitted = True

  return render(request, 'pages/contact.html',
                {'form': form,
                'page_list': mods.Page.objects.all(),
                'submitted': submitted})
"""

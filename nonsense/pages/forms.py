# -*- coding: utf-8 -*-
"""
Created on Fri Jan 12 20:06:18 2018

@author: eddy
"""

from django import forms
from datetime import date, timedelta

class ContactForm(forms.Form):
  subject = forms.CharField(max_length=100)
  email = forms.EmailField(required=False,label='Your email address')
  message = forms.CharField(widget=forms.Textarea)

class internetSpeedForm(forms.Form):

  initialdateto = date.today()
  initialdatefrom = initialdateto - timedelta(days=7)

  value = forms.ChoiceField(choices=[('ping', 'Ping'),
                                     ('download', 'Download'),
                                     ('upload', 'Upload')],
                            initial='download')
  yearsallowed = range(2017, date.today().year+1)
  datefrom = forms.DateField(widget=forms.SelectDateWidget(years=yearsallowed), initial=initialdatefrom, label='Date From')
  dateto = forms.DateField(widget=forms.SelectDateWidget(years=yearsallowed), initial=initialdateto, label='Date To')

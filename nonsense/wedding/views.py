from django.shortcuts import render
from django.http import HttpResponseRedirect

from .forms import eMailForm, partyForm
# Create your views here.


def index(request):

  return render(request, "wedding/index.html")



def rsvp(request):

  if request.method == 'POST':
    # Data has been submitted here already!
    pass
  else:
    form1 = eMailForm()
    form2 = ''


  return render(request, "wedding/rsvp.html", {'form1': form1, 'form2': form2})

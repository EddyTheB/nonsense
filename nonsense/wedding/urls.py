from django.conf.urls import url
from . import views

urlpatterns = [
  url(r'^$', views.index, name='wedding'),
  url(r'^rsvp', views.rsvp, name='wedding'),
]

from django.db import models

# Create your models here.
class guest(models.Model):
  firstName = models.CharField(max_length=30, blank=True)
  lastName = models.CharField(max_length=30, blank=True)
  email = models.EmailField(unique=True)
  attendingParty = models.BooleanField(default=False)
  invitedToCeremony = models.BooleanField(default=False)
  attendingCeremony = models.BooleanField(default=False)
  partnerFirstName = models.CharField(max_length=30, blank=True)
  partnerLastName = models.CharField(max_length=30, blank=True)
  partnerEmail = models.EmailField(blank=True)
  childrenNum = models.IntegerField(default=0)
  childrenNames = models.TextField("Children's Names", blank=True)
  performance = models.BooleanField(default=False)
  dietaryReqs = models.TextField("Dietary Requirements", blank=True)
  comments = models.TextField("Comments", blank=True)

  def __str__(self):
    return '{} {} - {}'.format(self.firstName, self.lastName, self.email)

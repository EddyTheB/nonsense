from django.contrib import admin
from .models import guest
from django.forms import Textarea
from django.db import models

class GuestAdmin(admin.ModelAdmin):
  list_display = ('lastName', 'firstName', 'email', 'attendingParty', 'invitedToCeremony', 'attendingCeremony')
  ordering = ('lastName', 'email')
  list_display_links = ('lastName', 'firstName', 'email')

  formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows':2, 'cols':40})},
    }


# Register your models here.
admin.site.register(guest, GuestAdmin)
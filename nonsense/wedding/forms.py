from django import forms

class eMailForm(forms.Form):
  email = forms.EmailField(label='your email')

class partyForm(forms.Form):
  firstName = forms.CharField(label='First Name', max_length=30)
  lastName = forms.CharField(label='Last Name', max_length=30)
  attendingParty = forms.BooleanField(label='Coming?', default=False)
  partnerFirstName = forms.CharField(max_length=30)
  partnerLastName = forms.CharField(max_length=30)
  partnerEmail = forms.EmailField(label="your partner's email")
  childrenNum = forms.IntegerField(default=0)
  childrenNames = forms.TextField(label="Children's Names")
  performance = forms.BooleanField(default=False)
  dietaryReqs = forms.TextField(label="Dietary Requirements")
  comments = forms.TextField(label="Comments")

"""
# Create your models here.
class guest(models.Model):
  firstName = models.CharField(max_length=30, blank=True)
  lastName = models.CharField(max_length=30, blank=True)
  email = models.EmailField(unique=True)
  attendingParty = models.BooleanField(default=False)
  invitedToCeremony = models.BooleanField(default=False)
  attendingCeremony = models.BooleanField(default=False)
  partnerFirstName = models.CharField(max_length=30, blank=True)
  partnerLastName = models.CharField(max_length=30, blank=True)
  partnerEmail = models.EmailField(blank=True)
  childrenNum = models.IntegerField(default=0)
  childrenNames = models.TextField("Children's Names", blank=True)
  performance = models.BooleanField(default=False)
  dietaryReqs = models.TextField("Dietary Requirements", blank=True)
  comments = models.TextField("Comments", blank=True)

  def __str__(self):
    return '{} {} - {}'.format(self.firstName, self.lastName, self.email)
"""
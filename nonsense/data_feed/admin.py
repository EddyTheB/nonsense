# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import DataFeed, internet_speed, met_obs

# Register your models here.
admin.site.register(DataFeed)
admin.site.register(internet_speed)
admin.site.register(met_obs)
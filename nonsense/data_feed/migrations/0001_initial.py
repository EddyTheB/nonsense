# Generated by Django 2.0 on 2018-01-21 11:51

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DataFeed',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('modelname', models.CharField(max_length=50)),
                ('xUnits', models.CharField(blank=True, max_length=20)),
                ('yUnits', models.CharField(blank=True, max_length=20)),
                ('xScaling', models.FloatField(blank=True, null=True)),
                ('yScaling', models.FloatField(blank=True, null=True)),
                ('xFieldName', models.CharField(max_length=50)),
                ('yFieldName', models.CharField(max_length=50)),
                ('colorByFieldName', models.CharField(blank=True, max_length=50)),
                ('hoverFieldName1', models.CharField(blank=True, max_length=50)),
                ('hoverFieldName2', models.CharField(blank=True, max_length=50)),
                ('hoverFieldName3', models.CharField(blank=True, max_length=50)),
                ('hoverFieldName4', models.CharField(blank=True, max_length=50)),
            ],
        ),
    ]

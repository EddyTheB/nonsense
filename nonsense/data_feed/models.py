# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class DataFeed(models.Model):
  modelname = models.CharField(max_length=50)  # The database table that stores the data.

  permalink = models.CharField(max_length=40, unique=True)
  update_date = models.DateTimeField('Last Updated')
  bodytext = models.TextField('Page Content', blank=True)

  xUnits = models.CharField(max_length=20, blank=True)
  yUnits = models.CharField(max_length=20, blank=True)
  xScaling = models.FloatField(blank=True, null=True)
  yScaling = models.FloatField(blank=True, null=True)
  xFieldName = models.CharField(max_length=50)
  yFieldName = models.CharField(max_length=50)
  colorByFieldName = models.CharField(max_length=50, blank=True)
  colorByPalette = models.CharField(max_length=50, blank=True, default='Category10')
  colorByPaletteReverse = models.BooleanField(default=False)
  hoverFieldName1 = models.CharField(max_length=50, blank=True)
  hoverFieldUnit1 = models.CharField(max_length=20, blank=True)
  hoverFieldScaling1 = models.FloatField(blank=True, null=True)
  hoverFieldName2 = models.CharField(max_length=50, blank=True)
  hoverFieldUnit2 = models.CharField(max_length=20, blank=True)
  hoverFieldScaling2 = models.FloatField(blank=True, null=True)
  hoverFieldName3 = models.CharField(max_length=50, blank=True)
  hoverFieldUnit3 = models.CharField(max_length=20, blank=True)
  hoverFieldScaling3 = models.FloatField(blank=True, null=True)
  hoverFieldName4 = models.CharField(max_length=50, blank=True)
  hoverFieldUnit4 = models.CharField(max_length=20, blank=True)
  hoverFieldScaling4 = models.FloatField(blank=True, null=True)

  def __str__(self):
    return '{} - {} v {}'.format(self.modelname, self.yFieldName, self.xFieldName)

class internet_speed(models.Model):
  server_id = models.BigIntegerField()
  sponser = models.CharField(max_length=50)
  server_name = models.CharField(max_length=30)
  timestamp = models.DateTimeField()
  distance = models.FloatField()
  ping = models.FloatField()
  download = models.FloatField()
  upload = models.FloatField()

  def __str__(self):
    return self.timestamp

class met_locations(models.Model):
  location_id = models.BigIntegerField()
  name = models.CharField(max_length=40)
  region = models.CharField(max_length=4, null=True)
  unitary_authority = models.CharField(max_length=30)
  observation_source = models.CharField(max_length=10, null=True)
  national_park = models.CharField(max_length=50, null=True)
  latitude = models.FloatField()
  longitude = models.FloatField()
  elevation = models.FloatField()

  def __str__(self):
    return '{}: {}'.format(self.location_id, self.name)

class met_obs(models.Model):
  location_id = models.BigIntegerField()
  timestamp = models.DateTimeField()
  weather_type = models.SmallIntegerField(null=True)
  pressure = models.SmallIntegerField(null=True)
  pressure_tendency = models.CharField(max_length=4, blank=True)
  wind_speed = models.FloatField(null=True)
  wind_gust = models.FloatField(null=True)
  wind_direction = models.CharField(max_length=3, blank=True)
  temperature = models.FloatField(null=True)
  dew_point = models.FloatField(null=True)
  visibility = models.BigIntegerField(null=True)
  updated = models.DateTimeField()

  def __str__(self):
    return '{} {}'.format(self.location_id, self.timestamp)

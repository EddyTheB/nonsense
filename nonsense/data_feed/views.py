# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime, timedelta
import os
import numpy as np
import pandas as pd
from pandas.api.types import is_string_dtype
from math import pi
import importlib

from django.shortcuts import render, get_object_or_404
from django.conf import settings

from bokeh.plotting import figure, ColumnDataSource
from bokeh.resources import CDN
import bokeh.palettes as palettes
from bokeh.embed import components
from bokeh.models import DatetimeTickFormatter, Range1d, HoverTool, ColorBar
from bokeh.transform import linear_cmap

from pages.models import Page
from data_feed.models import DataFeed
import data_feed.models as mods
import meteorology.models as mods_met
f = getattr(mods, 'internet_speed')
#m = importlib.import_module(settings.MYCLASSES['internet_speed'])

def index(request):
  context = {
    'page_list': Page.objects.all(),
    'data_list': DataFeed.objects.all(),
    }
  return render(request, "data_feed/index.html", context)

def singleplot(request, pagename):

  #pagename = '/' + pagename
  df = get_object_or_404(DataFeed, permalink=pagename)
  title = df.__str__
  dateto = datetime.now()
  datefrom = dateto - timedelta(days=7)
  # Extract the data from the database.
  try:  
    model = getattr(mods, df.modelname)
  except AttributeError:
    model = getattr(mods_met, df.modelname)

  color_bar = None
  data = model.objects.all()

  data = pd.DataFrame.from_records(data.values())
  data['x_blahblah'] = data[df.xFieldName]
  data['y_blahblah'] = data[df.yFieldName]
  if df.xScaling is not None:
    data['x_blahblah'] *= df.xScaling
  if df.yScaling is not None:
    data['y_blahblah'] *= df.yScaling
  if df.hoverFieldName1 != '':
    data['hover_blahblah1'] = data[df.hoverFieldName1]
    if df.hoverFieldScaling1 is not None:
      data['hover_blahblah1'] *= df.hoverFieldScaling1
  if df.hoverFieldName2 != '':
    data['hover_blahblah2'] = data[df.hoverFieldName2]
    if df.hoverFieldScaling2 is not None:
      data['hover_blahblah2'] *= df.hoverFieldScaling2
  if df.hoverFieldName3 != '':
    data['hover_blahblah3'] = data[df.hoverFieldName3]
    if df.hoverFieldScaling3 is not None:
      data['hover_blahblah3'] *= df.hoverFieldScaling3
  if df.hoverFieldName4 != '':
    data['hover_blahblah4'] = data[df.hoverFieldName4]
    if df.hoverFieldScaling4 is not None:
      data['hover_blahblah4'] *= df.hoverFieldScaling4
  if df.colorByFieldName != '':
    try:
      palette = getattr(palettes, df.colorByPalette)
    except AttributeError:
      palette = getattr(palettes, 'Category10')
    p_reverse = df.colorByPaletteReverse
    min_num_in_palette = min(list(palette.keys()))
    max_num_in_palette = max(list(palette.keys()))

    data['c_word'] = data[df.colorByFieldName]
    if is_string_dtype(data['c_word']):
      # It's a string. How many unique values are there.
      n_unique_c = data['c_word'].nunique()
      # Select the appropriate palette.
      if n_unique_c < min_num_in_palette:
        palette = palette[min_num_in_palette]
      else:
        palette = palette[min([n_unique_c, max_num_in_palette])]
      if p_reverse:
        palette = palette[::-1]
      # Categorize the colors.
      data['c_word'] = data['c_word'].astype('category')
      data['c_code'] = data['c_word'].cat.codes
      data['c_blahblah'] = data['c_code'].apply(lambda x: palette[x%(max_num_in_palette-1)])
      #title = data[['c_word', 'c_code', 'c_blahblah']].head()
      #title = '{} - {} - {}'.format(n_unique_c, min_num_in_palette, max_num_in_palette)
    else:
      # It's numeric, presumably.
      palette = palette[max_num_in_palette]
      if p_reverse:
        palette = palette[::-1]
      mapper = linear_cmap(field_name='c_word',
                           palette=palette,
                           low=data['c_word'].min(),
                           high=data['c_word'].max())
      color_bar = ColorBar(color_mapper=mapper['transform'], width=8, location=(0,0))
  else:
    data['c_blahblah'] = 'firebrick'


  plot = figure(toolbar_location='below',
                plot_width=760, plot_height=500,
                tools="xpan,xwheel_zoom,reset")
  if color_bar is not None:
    plot.circle(x='x_blahblah', y='y_blahblah', line_color='black', fill_color=mapper, source=data) #, legend='c_word')
    plot.add_layout(color_bar, 'right')
  else:
    plot.circle('x_blahblah', 'y_blahblah', color='c_blahblah', source=data)
  #plot.circle('x_blahblah', 'y_blahblah', source=data)

  #c = 'firebrick'
  

  plot.x_range = Range1d(datefrom, dateto)

  if df.xUnits is not None:
    plot.xaxis.axis_label = df.xUnits
  if df.yUnits is not None:
    plot.yaxis.axis_label = df.yUnits

  plot.xaxis.formatter=DatetimeTickFormatter(hours=["%d %B %Y"],
                                             days=["%d %B %Y"],
                                             months=["%d %B %Y"],
                                             years=["%d %B %Y"],)


  #plot.legend.location = "bottom_left"
  plot.xaxis.major_label_orientation = pi/2
  plot.add_tools(HoverTool())

  # Hover tool.
  hover = plot.select(dict(type=HoverTool))
  hoverToolTips = []
  if df.hoverFieldName1 != '':
    if df.hoverFieldUnit1 == '':
      hovername = df.hoverFieldName1
    else:
      hovername = '{} ({})'.format(df.hoverFieldName1, df.hoverFieldUnit1)
    hoverToolTips.append((hovername, "@hover_blahblah1"))
  if df.hoverFieldName2 != '':
    if df.hoverFieldUnit2 == '':
      hovername = df.hoverFieldName2
    else:
      hovername = '{} ({})'.format(df.hoverFieldName2, df.hoverFieldUnit2)
    hoverToolTips.append((hovername, "@hover_blahblah2"))
  if df.hoverFieldName3 != '':
    if df.hoverFieldUnit3 == '':
      hovername = df.hoverFieldName3
    else:
      hovername = '{} ({})'.format(df.hoverFieldName3, df.hoverFieldUnit3)
    hoverToolTips.append((hovername, "@hover_blahblah3"))
  if df.hoverFieldName4 != '':
    if df.hoverFieldUnit4 == '':
      hovername = df.hoverFieldName4
    else:
      hovername = '{} ({})'.format(df.hoverFieldName4, df.hoverFieldUnit4)
    hoverToolTips.append((hovername, "@hover_blahblah4"))
  hover.tooltips = hoverToolTips

  script, div = components(plot, CDN)


  context = {
    'title': title, #df.__str__,
    'content': df.bodytext,
    'last_updated': df.update_date,
    'page_list': Page.objects.all(),
    'the_script': script,
    'the_div': div
    }
  return render(request, "data_feed/datafeed.html", context)
